from django.contrib import admin
from .models import Book, Author, Publisher # 해당하는 위치에 models에서 각 테이블을 임포트


admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Publisher)

