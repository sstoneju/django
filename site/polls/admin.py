from django.contrib import admin

# Register your model here.
from .models import Question, Choice


# class ChoiceInLine(admin.StackInline):
class ChoiceInLine(admin.TabularInline):
    model = Choice
    extra = 2

class QuestionAdmin(admin.ModelAdmin):
    # fieldsets = ['pub_date', 'question_text']
    fieldsets = [
        ('Question Statement' , {'fields': ['question_text']}),
        ('Date Information', {'fields' : ['pub_date'] , 'classes' : ['collapse']}),
    ]
    inlines = [ChoiceInLine]  # choice 같이 보기
    list_display = ('question_text' , 'pub_date')   # 레코드 리스트 칼럼 항목 지전
    list_filter = ['pub_date']  # 필터 사이드 바 추가
    search_fields =  ['question_text']  # 검색 박스 추가

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)



